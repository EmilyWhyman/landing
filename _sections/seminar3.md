---
title: Term 3
icon: fa-th
order: 5
---

{% for course in site.seminar3 %}
 [{{course.title}}]({{course.url|relative_url}})
{% endfor %}
